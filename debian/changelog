libmath-symbolic-perl (0.613-1) unstable; urgency=medium

  * Import upstream version 0.613.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Add /me to Uploaders.
  * Remove TODO from previous changelog entry.

 -- gregor herrmann <gregoa@debian.org>  Mon, 16 Sep 2024 19:55:00 +0200

libmath-symbolic-perl (0.612-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Andrius Merkys ]
  * Regenerating lib/Math/Symbolic/Parser/Yapp.pm from Yapp.yp
    (Closes: #904024).
  * Bumping compat level.
  * Removing embedded Parse::Yapp::Driver from
    lib/Math/Symbolic/Parser/Yapp.pm.
  * Adding dependency on libparse-yapp-perl as Parse::Yapp::Driver is no
    longer included.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:12:00 +0100

libmath-symbolic-perl (0.612-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 15:28:22 +0100

libmath-symbolic-perl (0.612-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 May 2015 23:55:37 +0200

libmath-symbolic-perl (0.612-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 0.612
  * Update copyright years for upstream files
  * Drop fix-pod-spelling.patch patch
  * Drop fix-pod-encoding.patch pach
  * Swap order of alternative Build-Depends fo Module::Build.
    Change alternative Build-Depends needed for Module::Build requirement to
    perl (>= 5.17.1) | libmodule-build-perl (>= 0.400000).
  * Add fix-manpage-has-errors-from-pod2man.patch patch.
    Add missing encoding causing otherwise an pod2man error when generating
    manpage for Math::Symbolic::Parser::Precompiled.

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 23 Oct 2013 23:12:27 +0200

libmath-symbolic-perl (0.609-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
    Fixes "FTBFS with perl 5.18: POD"
    (Closes: #710843)
  * Refresh spelling patch (offset, one more correction).
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Build-depend on Module::Build 0.40.
  * Set Standards-Version to 3.9.4 (no changes).
  * Drop packages from Build-Depends-Indep that are not used anymore
    during tests.
  * Add a patch to fix two remaining PODs.

 -- gregor herrmann <gregoa@debian.org>  Mon, 03 Jun 2013 19:02:47 +0200

libmath-symbolic-perl (0.606-1) unstable; urgency=low

  * Initial Release (Closes: #608565)

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 01 Jan 2011 10:32:06 -0500
